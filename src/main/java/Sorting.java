import java.util.Arrays;
import java.util.Scanner;

public class Sorting {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(getInput("How many integers do you want to input(up to 10): "));
        int[] numbers = new int[n];

        for (int i = 0; i < n; i++) {
            numbers[i] = scanner.nextInt();
        }
        sort(numbers);

        System.out.println(Arrays.toString(numbers));
    }

    public static String getInput(String text) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(text);
        return scanner.nextLine();
    }


    public static void sort(int[] numbers) {
        if (numbers == null || numbers.length > 10) {
            throw new IllegalArgumentException();
        }

        int length = numbers.length;
        boolean needIteration = true;

        while (needIteration) {
            needIteration = false;
            for (int i = 0; i < length - 1; i++) {
                for (int j = 0; j < length - 1 - i; j++) {
                    if (numbers[j] > numbers[j + 1]) {
                        int temp = numbers[j];
                        numbers[j] = numbers[j + 1];
                        numbers[j + 1] = temp;
                        needIteration = true;
                    }
                }
            }
        }
    }


}
