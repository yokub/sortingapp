import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class SortingCornerCasesTest {
    @Test(expected = IllegalArgumentException.class)
    public void testNullCase(){
        Sorting.sort(null);
    }

    @Test
    public void testEmptyCase(){
        int[] ints = new int[]{};
        Sorting.sort(ints);

        assertArrayEquals(new int[]{}, ints);
    }

    @Test
    public void testSingleElementArrayCase() {
        int[] ints = {1};
        Sorting.sort(ints);

        assertArrayEquals(new int[]{1}, ints);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testMoreThanTenElementsCase() {
        Sorting.sort(new int[]{12,11,10,9,8,7,6,5,4,3,2,1});
    }
}
