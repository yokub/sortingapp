import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingTest  {

    private int[] numbers;
    private int[] expected;

    public SortingTest(int[] numbers, int[] expected) {
        this.numbers = numbers;
        this.expected = expected;
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{5,1,3}, new int[]{1,3,5}},
                {new int[]{2,5,4,1,3}, new int[]{1,2,3,4,5}},
                {new int[]{6,5,1,3,7,4,2}, new int[]{1,2,3,4,5,6,7}},
                {new int[]{9,1,2,7,5,3,6,8,4,10}, new int[]{1,2,3,4,5,6,7,8,9,10}},
        });
    }

    @Test
    public void testSorting() {
        Sorting.sort(numbers);
        assertArrayEquals(expected, numbers);
    }
}